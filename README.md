# README #

The database and website database for Hook Game

Currently features movement and a grappling hook

# CONTROLS #

A/D - movement

Space - jump

Mouse - hook

W/S - retract/extend hook

# OVERALL PLAN #

https://docs.google.com/document/d/1OoKZrYrEmDCWbElCuoFfxbidFvuwt6PM9SOy9F495po/edit